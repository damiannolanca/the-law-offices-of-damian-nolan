Attorney Damian M. Nolan is an experienced and aggressive family law attorney in Long Beach CA. He has litigated more than 1,000 family law matters involving divorce and child custody cases throughout Long Beach, Lakewood, & Los Angeles County.

Address: 444 W Ocean Blvd, #800, Long Beach, CA 90802, USA

Phone: 562-634-1113

Website: http://www.losangeles-orangecountylawyer.com
